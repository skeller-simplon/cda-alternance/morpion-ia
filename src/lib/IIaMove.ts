import IPos from "./IPos";

export default interface IIaMove {
    pos: IPos,
    player: string,
    score: number
}