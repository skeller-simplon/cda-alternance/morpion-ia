import GameCheck from "./GameCheck";
import GameDrawer from "./GameDrawer";
import GameIA from "./GameIA";
import IIaMove from "./lib/IIaMove";
import IPos from "./lib/IPos";

export default class Game extends GameDrawer {
    public grid: string[][] = [];
    private gridSize = 4;
    private aiSign: string = "X"
    private playerSign: string = "0"
    private isIaOnlyGame: boolean;

    constructor(isIaOnlyGame: boolean) {
        super()
        this.isIaOnlyGame = isIaOnlyGame;
    }

    start() {
        this.initGrid();
        this.drawGrid(this.grid, this.gridSize, this.addMark)
        this.drawActions()
    }

    private playNormalRound(pos: IPos) {
        let status = this.addMark(pos, this.playerSign);

        if (status === null) {
            const ia: GameIA = new GameIA(this.playerSign, this.aiSign, this.gridSize)
            let iaPos: IIaMove = ia.fetchBestMove(this.grid, this.aiSign)
            status = this.addMark(iaPos.pos, this.aiSign)
        }
        if (status !== null) {
            this.endGame(status)
        }
    }

    private playIaVsIaRound() {
        let status;
        setTimeout(() => {
            const iaIa: GameIA = new GameIA(this.playerSign, this.aiSign, this.gridSize)
            let iaIaPos: IIaMove = iaIa.fetchBestMove(this.grid, this.aiSign)
            status = this.addMark(iaIaPos.pos, this.aiSign)
        }, 2000)


        if (status === null) {
            setTimeout(() => {
                const playerIa: GameIA = new GameIA(this.aiSign, this.playerSign, this.gridSize)
                let iaPlayer: IIaMove = playerIa.fetchBestMove(this.grid, this.playerSign)
                status = this.addMark(iaPlayer.pos, this.playerSign)
            }, 2000)

        }
        if (status !== null) {
            this.endGame(status)
        }
    }

    private endGame(winner: string) {
        console.log(winner);

        if (winner === "DRAW")
            alert("draw")
        else
            alert(winner === "O" ? "player" : "ia" + " WON")
        this.start();
    }

    private initGrid() {
        let grid = []
        for (let x = 0; x < this.gridSize; x++) {
            grid[x] = []
            for (let y = 0; y < this.gridSize; y++) {
                grid[x][y] = null;
            }
        }
        this.grid = grid;
    }

    private drawActions() {
        if (this.isIaOnlyGame)
            this.playIaVsIaRound()
        else
            for (let x = 0; x < this.gridSize; x++) {
                for (let y = 0; y < this.gridSize; y++) {
                    document.getElementById(this.getUniqueId(x, y)).addEventListener("click", (e) => {
                        if (this.grid[x][y] === null)
                            this.playNormalRound({ x: x, y: y } as IPos)
                    })
                }
            }
    }


    private addMark(pos: IPos, val: string): string {
        this.grid[pos.x][pos.y] = val;
        this.drawSquare(pos, val)
        return new GameCheck(this.grid, this.gridSize).checkForWinner();
    }
}