export default class GameCheck {
    grid: string[][];
    gridSize: number;
    winningStrokes: number = 3;

    constructor(grid: string[][], gridSize: number) {
        this.grid = grid;
        this.gridSize = gridSize;
    }

    /**
     * ret sera rempli par soit la valeur d'un des joueurs soit par "DRAW"
     */
    public checkForWinner(): string {
        let ret = null;
        if (!ret)
            ret = this.checkRows()
        if (!ret)
            ret = this.checkColumns()
        if (!ret)
            ret = this.checkDiagonals()

        if (!ret)
            ret = this.checkDraw();
        return ret;
    }
    private checkRows() {
        for (const row of this.grid) {
            if (this.checkWinnableArray(row))
                return row[0]
        }
        return null
    }

    private checkColumns() {
        for (let y = 0; y < this.gridSize; y++) {
            let col = []
            for (let x = 0; x < this.gridSize; x++) {
                col.push(this.grid[x][y])
            }
            if (this.checkWinnableArray(col))
                return col[0]
        }
        return null
    }

    private checkDiagonals() {
        // Vérification diagnole
        let dia = []
        for (let x = 0; x < this.gridSize; x++)
            dia.push(this.grid[x][x])

        // Vérification diagonale "inversée"
        let invDia = []

        for (let x = 0; x < this.gridSize; x++)
            invDia.push(this.grid[this.gridSize - x - 1][x])

        if (this.checkWinnableArray(dia))
            return dia[0]
        if (this.checkWinnableArray(invDia))
            return invDia[0]

        return null
    }

    private checkDraw() {
        let counter = 0

        for (let x = 0; x < this.gridSize; x++) {
            for (let y = 0; y < this.gridSize; y++) {
                if (this.grid[x][y] !== null)
                    counter++
            }
        }
        return counter === Math.pow(this.gridSize, 2) ? "DRAW" : null;
    }

    /**
     * Vérifie s'il existe une combinaison gagnante depuis une tranche de la grille.
     * @param arr tableau de coups concomitants
     */
    private checkWinnableArray(arr: string[]) {
        let possiblyWinningArray = [arr[0]];
        for (let i = 1; i < arr.length; i++) {
            if (arr[i] === possiblyWinningArray[0]) {
                possiblyWinningArray.push(arr[i]);
                if (possiblyWinningArray.length === this.winningStrokes)
                    return possiblyWinningArray[0]
            } else {
                possiblyWinningArray = [arr[i]]
            }
        }
        return null;
    }
}