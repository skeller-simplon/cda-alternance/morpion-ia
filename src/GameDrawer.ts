import IPos from "./lib/IPos";

export default class GameDrawer {

    protected drawGrid(grid: string[][], gridSize: number, clickEvent: any) {
        const table = document.querySelector("table");
        table.innerHTML = "";
        for (let x = 0; x < gridSize; x++) {
            let row = table.insertRow();
            for (let y = 0; y < gridSize; y++) {
                let th = document.createElement("td");
                th.id = this.getUniqueId(x, y)
                row.appendChild(th);
            }
        }
    }
    protected drawSquare(pos: IPos, val: string) {
        document.getElementById(this.getUniqueId(pos.x, pos.y)).innerText = val
    }

    protected getUniqueId(x: number, y: number): string {
        return "case-" + x + "-" + y;
    }
}