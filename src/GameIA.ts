import GameCheck from "./GameCheck";
import IIaMove from "./lib/IIaMove";
import IPos from "./lib/IPos";

export default class GameIA {

    private aiSign: string;
    private playerSign: string;
    private gridSize: number;

    constructor(playerSign, aiSign, gridSize) {
        this.aiSign = aiSign;
        this.playerSign = playerSign;
        this.gridSize = gridSize
    }

    /**
     * Evalue récursivement le meilleur coup à jouer en regardant la liste des
     * enchainements de coups possibles.
     * @param grid grille à évaluer
     * @param player joueur faisant le coup
     */
    fetchBestMove(grid: string[][], player: string): IIaMove {
        let checker = new GameCheck(grid, this.gridSize)

        // Jouer ce coup la fait gagner l'ia => Max score 10
        if (checker.checkForWinner() === this.aiSign)
            return { player: this.aiSign, score: 10 } as IIaMove;
        // Fait perder l'ia => min score -10
        else if (checker.checkForWinner() === this.playerSign)
            return { player: this.playerSign, score: -10 } as IIaMove
        else if (checker.checkForWinner() === "DRAW")
            return { player: this.playerSign, score: 0 } as IIaMove

        let moves: IIaMove[] = []

        for (let x = 0; x < this.gridSize; x++) {
            for (let y = 0; y < this.gridSize; y++) {
                if (grid[x][y] === null) {
                    let move: IIaMove = {
                        pos: { x: x, y: y },
                        player: player
                    } as IIaMove;

                    // On assume le coup joué pour descendre dans l'arbre
                    grid[x][y] = player;

                    if (player === this.aiSign) {
                        let result = this.fetchBestMove(grid, this.playerSign)
                        move.score = result.score;
                    } else {
                        let result = this.fetchBestMove(grid, this.aiSign)
                        move.score = result.score;
                    }
                    // Reset de la grille
                    grid[x][y] = null;

                    // Ajout à la liste des moves.
                    moves.push(move)
                }
            }
        }

        let bestMove: IIaMove = this.getBestMove(player, moves);

        return bestMove
    }

    /**
     * Renvoie le meilleur coup à jour pour l'IA depuis un tableau
     * @param player joueur
     * @param moves mouvements à checker
     */
    private getBestMove(player: string, moves: IIaMove[]) {
        let bestMove: IIaMove;
        // Si c'est le move de l'IA => Choisis le meilleur pour lui
        if (player === this.aiSign) {
            let bestScore = -10000;
            for (const move of moves)
                if (move.score > bestScore) {
                    bestScore = move.score;
                    bestMove = move;
                }
            // Si c'est le move du joueur => Choisis le pire pour lui
        } else {
            let bestScore = 10000;
            for (const move of moves)
                if (move.score < bestScore) {
                    bestScore = move.score;
                    bestMove = move;
                }
        }
        return bestMove;
    }
}