import Game from "./Game";


let elem = document.getElementById("app")

let button = document.createElement("button")
button.type = "button";
button.innerText = "Joueur vs IA"
button.addEventListener("click", () => {
    new Game(false).start()
})
elem.appendChild(button)


let buttonIA = document.createElement("button")
buttonIA.type = "button";
buttonIA.innerText = "IA vs IA"
buttonIA.addEventListener("click", () => {
    new Game(true).start()
})
elem.appendChild(buttonIA)
